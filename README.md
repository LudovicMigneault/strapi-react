# Authentication Flow Example

Basic app that shows how the implement authentication using `JSON Web Token`.


# TODOs

- Changer offline-plugin's node_modules\offline-plugin\lib\misc 
pour ajouter le support des notifications. 
(Recherches en ligne pour voir comment apporter des changements a un package npm)

- Ajouter support pour Material UI

- Importer code crée du projet material UI

- Lire Doc : https://github.com/react-boilerplate/react-boilerplate

- Comprendre Saga pour les requests : app/containers/AuthPage/saga.js